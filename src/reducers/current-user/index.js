import immutable from 'immutability-helper';
import createReducer from '../../helpers/reducer/create-reducer';
import authenticate from '../../services/auth';
import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS } from '../../actions/auth';

const initialState = {
  data: {},
  loading: false,
  errors: null
};

const loginSuccessHandler = (state, payload) => {
  return { $set: payload };
};

const loginFailureHandler = (state, payload) => {
  return { $set: payload };
};

export default {
  currentUser: createReducer(initialState, {
    [LOGIN_SUCCESS](state, { payload }) {
      return immutable(state, {
        data: loginSuccessHandler(state, payload)
      });
    },
    [LOGIN_FAILURE](state, { payload }) {
      return immutable(state, {
        errors: loginFailureHandler(state, payload)
      });
    },
    [LOGOUT_SUCCESS]() {
      return initialState;
    }
  })
};
