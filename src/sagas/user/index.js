import { all, call, put, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { LOGIN, loginSuccess, loginFailure } from '../../actions/auth';
import authenticate from '../../services/auth';

export function* loginSaga({ payload }): Generator<*, *, *> {
  try {
    const response = yield call(authenticate, payload);

    yield put(loginSuccess(response));
  } catch (err) {
    yield put(loginFailure(err));
  }
}

export default function* root(): Generator<*, *, *> {
  yield all([takeEvery(LOGIN, loginSaga)]);
}
