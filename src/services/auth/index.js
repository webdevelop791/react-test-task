import users from '../../fake-data/users';

const authenticate = ({ login, password }) => {
  const user = users.find(user => user.login === login && user.password === password);

  if (user) {
    return user;
  }

  throw ['Login or password incorrect'];
};

export default authenticate;
