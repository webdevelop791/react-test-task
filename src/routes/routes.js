import HomePage from '../pages/home';
import LoginPage from '../pages/login';
import ProfilePage from '../pages/profile';
import { HOME_PATH, LOGIN_PATH, PROFILE_PATH } from './constants';

const routes = [
  {
    path: HOME_PATH,
    key: HOME_PATH,
    component: HomePage,
    exact: true
  },
  {
    path: LOGIN_PATH,
    key: LOGIN_PATH,
    component: LoginPage,
    exact: true
  },
  {
    path: PROFILE_PATH,
    key: PROFILE_PATH,
    component: ProfilePage,
    exact: true
  }
];

export default routes;
