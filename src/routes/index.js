import * as React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import PageWrap from '../components/page-wrap';
import routes from './routes';

class AppRoutes extends React.Component {
  renderRoutes = () => {
    return routes.map(route => <Route {...route} />);
  };

  render() {
    return (
      <Router>
        <Switch>
          <PageWrap>{this.renderRoutes()}</PageWrap>
        </Switch>
      </Router>
    );
  }
}

export default AppRoutes;
