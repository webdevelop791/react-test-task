import * as React from 'react';
import { Menu, Layout } from 'antd';
import { Link } from 'react-router-dom';
import { HOME_PATH, PROFILE_PATH } from '../../routes/constants';

const { Item } = Menu;
const { Header } = Layout;

const Nav = ({ logoutHandler }) => (
  <Menu
    theme="dark"
    mode="horizontal"
    defaultSelectedKeys={[HOME_PATH]}
    style={{ lineHeight: '64px' }}
  >
    <Item key={HOME_PATH}>
      <Link to={HOME_PATH}>Home</Link>
    </Item>
    <Item key={PROFILE_PATH}>
      <Link to={PROFILE_PATH}>Profile</Link>
    </Item>
    <Item key="logout" onClick={logoutHandler}>
      Logout
    </Item>
  </Menu>
);

const HeaderNav = ({ isAuthed, logoutHandler }) => (
  <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
    {isAuthed ? <Nav logoutHandler={logoutHandler} /> : null}
  </Header>
);

export default HeaderNav;
