import * as React from 'react';
import { connect } from 'react-redux';
import { Form, Input, Button } from 'antd';
import authenticate from '../../services/auth';
import { login } from '../../actions/auth';

const { Item } = Form;

const hasErrors = errors => {
  console.log(errors);
  return false;
};

class LoginForm extends React.Component {
  componentDidMount() {
    // this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        return this.props.dispatch(login(values));
      }
    });
  };

  renderAuthErrors = errors => errors.map(error => <p key={error}>{error}</p>);

  render() {
    const { user, form } = this.props;
    const { getFieldDecorator, getFieldsError } = form;

    return (
      <div className="login-form-container">
        <div className="login-form-title">
          <h2>Login</h2>
          {user.errors && this.renderAuthErrors(user.errors)}
        </div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Item>
            {getFieldDecorator('login', {
              rules: [{ required: true }]
            })(<Input placeholder="Username" />)}
          </Item>
          <Item>
            {getFieldDecorator('password', {
              rules: [{ required: true }]
            })(<Input type="password" placeholder="Password" />)}
          </Item>
          <Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
          </Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.currentUser
});

export default connect(mapStateToProps)(Form.create()(LoginForm));
