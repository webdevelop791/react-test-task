import * as React from 'react';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';
import LoginPage from '../../pages/login';
import HeaderNav from '../header-nav';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

class PageWrap extends React.PureComponent {
  logoutHandler = () => this.props.dispatch(logout());

  render() {
    const { children, user } = this.props;
    const isAuthed = user.data.firstname;
    const content = isAuthed ? children : <LoginPage />;

    return (
      <Layout>
        <HeaderNav isAuthed={isAuthed} logoutHandler={this.logoutHandler} />
        <Content style={{ padding: '0 50px', marginTop: 64 }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>{content}</div>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  user: state.currentUser
});

export default connect(mapStateToProps)(PageWrap);
