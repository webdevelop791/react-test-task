export const LOGIN = '/auth/login';
export const login = payload => ({
  type: LOGIN,
  payload
});

export const LOGIN_SUCCESS = '/auth/login/success';
export const loginSuccess = payload => ({
  type: LOGIN_SUCCESS,
  payload
});

export const LOGIN_FAILURE = '/auth/login/failure';
export const loginFailure = payload => ({
  type: LOGIN_FAILURE,
  payload
});

export const LOGOUT_SUCCESS = '/auth/logout/success';
export const logout = () => ({
  type: LOGOUT_SUCCESS
});
