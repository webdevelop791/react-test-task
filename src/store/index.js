import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import 'regenerator-runtime/runtime';
import createSagaMiddleware from 'redux-saga';
import reducers from '../reducers';
import sagas from '../sagas';

export default function configureStore() {
  const logger = createLogger();
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [thunkMiddleware, logger, sagaMiddleware];

  const persistConfig = {
    storage,
    key: '18175709887651',
    whitelist: ['currentUser']
  };

  const reducer = persistCombineReducers(persistConfig, reducers);
  const store = createStore(reducer, applyMiddleware(...middlewares));
  const persistor = persistStore(store);

  sagaMiddleware.run(sagas);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      /* eslint-disable global-require */
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }
  return { persistor, store };
}
