import * as React from 'react';
import { withRouter } from 'react-router-dom';
import LoginForm from '../../components/login-form';

class LoginPage extends React.Component {
  render() {
    return (
      <div>
        <LoginForm />
      </div>
    );
  }
}

export default withRouter(LoginPage);
