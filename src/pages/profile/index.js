import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class ProfilePage extends React.Component {
  getUserName = () => {
    const { user } = this.props;

    return `${user.data.firstname} ${user.data.lastname}`;
  };

  render() {
    const { user } = this.props;
    console.log(this.props);

    return <div>Hello "{this.getUserName()}"</div>;
  }
}

const mapStateToProps = state => ({
  user: state.currentUser
});

export default withRouter(connect(mapStateToProps)(ProfilePage));
