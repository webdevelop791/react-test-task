Install:

1. Clone repository;
2. Go to project path;
3. Run "npm install";
4. Run "npm start";

Note: All available users you can see at /src/fake-data/users. 

Enjoy.
